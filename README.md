# Substrate ABnet

## What is it?

This repository contains Ansible code for deploying a Substrate-based network on Google Compute Platform.
The network runs the "Alice and Bob" example built in to
[substrate-node-template](https://github.com/substrate-developer-hub/substrate-node-template/).

Specifically the code creates and configures:

* a VPC network
* firewall rules for the network
* three external IP addresses
* one GCP instance for running [substrate-telemetry](https://github.com/paritytech/substrate-telemetry)
* one GCP instance for running the "Alice" node
* one GCP instance for running the "Bob" node

## How do I use it?

You need an account on [Google Cloud Platform](https://console.cloud.google.com/) and an empty project that you can use.
Note that the project ID can be different from the name you chose. Open the file [vars/all](vars/all) and replace the
value for `gce_project` with your project ID. You can also change some other parameters here, like the region and zone
where the GCP resources will be provisioned.
After creating the project, [download credentials for a service account](https://console.cloud.google.com/apis/credentials/serviceaccountkey).
Make sure the correct project is selected on that page. You can use the Compute Engine default service account. Put the
credentials in the directory where you cloned this repository to and name it `credentials.json`, or put it somewhere
else and adjust the `gcp_service_account_file` in [vars/all](vars/all).

If you don't have Ansible installed on your machine, you can use the script `create_ansible_env.sh` to install it in a
Python virtualenv:
```
$ ./create_ansible_env.sh
$ source venv/bin/activate
```

Run the playbook with `ansible-playbook -i inventory site.yml`.

Once it has finished successfully, you should be able to access the telemetry UI by putting the external IP address of
the telemetry node in your browsers address bar.
