#!/usr/bin/env bash

set -e

python -m venv ./venv
./venv/bin/pip install -U -qqq pip
./venv/bin/pip install -qqq -r requirements.txt
